summary: Verifies attempting to violate the kernel memory address space is prevented.
description: |
    Verifies attempting to violate the kernel memory address space is prevented and results in a failure (e.g. kernel panic).
    This test attempts to access a  read-only page.
    Test Inputs:
        module source, violate_kernel_memory.c
        trigger, echo 1 > /sys/kernel/vkm/write_ro_crash
        capture, dmesg > dmesg-crash.log
        check, rlAssertGrep "Unable to handle kernel write to read-only memory" dmesg-crash.log
    Expected results:
        [   PASS   ] :: (SEGFAULT expected) (Expected 139, got 139)
        [   PASS   ] :: File 'dmesg-crash.log' should contain 'Unable to handle kernel write to read-only memory'
    Results location:
        output.txt
contact: Stephen Bertram <sbertram@redhat.com>
component:
  - kernel
test: bash ./runtest.sh
id: a4f0bd8a-9507-4ce8-8766-1304e59ca590
framework: beakerlib
require:
  - make
  - iputils
  - type: file
    pattern:
      - /memory/mmra/violate_kernel_memory/src
      - /kernel-include
duration: 10m
