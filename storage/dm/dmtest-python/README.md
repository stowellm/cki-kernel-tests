# storage/lvm/device-mapper
Simple test wrapper to run [Device Mapper Test Suite][01].

[01]: https://github.com/jthornber/dmtest-python

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
